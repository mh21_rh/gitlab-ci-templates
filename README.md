# Gitlab Templates

Repository to store Gitlab CI templates for Kernel QE.

This repository contains common jobs, and it's internally mirrored
at [kernel-qe/gitlab-ci-templates].

IBy following the [DRY pattern], we're [templates].

By default, the following stages are defined:

* setup
* lint
* test
* build
* publish

## Template files

Every file should be responsible for a **single functionality**.
By doing this, we are keeping our files clean, readable, and maintainable.
All templates files are under *.gitlab* directory

```bash
$ tree .gitlab
.gitlab
├── all_templates.yml
├── base_templates
│   ├── helpers.yml
│   ├── stages.yml
│   ├── use_gitlab_cee_shared_runners.yml
│   ├── use_kernel_qe_internal_runners.yml
│   ├── variables.yml
│   └── workflow.yml
└── ci_templates
    ├── bash.yml
    ├── python.yml
    ├── common.yml
    ├── tmt.yml
```

## Common information

All templates use the prefix `kqe_` to define a namespace avoiding conflict with templates names.

All templates are executed with containers, there are two variables for every job.

* `<JOB_TEMPLATE_NAME>`_CONTAINER_IMAGE
* `<JOB_TEMPLATE_NAME>`_CONTAINER_VERSION

If you want to replace any container image or version, you only need to change any of this variables.

For example in `.kqe_markdownlint`, you should overwrite this variables with your custom values.

* KQE_MARKDOWNLINT_CONTAINER_IMAGE
* KQE_MARKDOWNLINT_CONTAINER_VERSION

Currently, we're using two main conainers images for all templates, these are the variables used:

* CKI_TOOLS_CONTAINER_IMAGE
* CKI_TOOLS_CONTAINER_VERSION
* KQE_TOOLS_CONTAINER_IMAGE
* KQE_TOOLS_CONTAINER_VERSION

In most of the jobs, can check only the files related to the merge requests or all files.
This is handle by the variable `CHECK_ONLY_MR_CHANGES`, the default value is `false`, but
every job change use a different value. This variable only affects to merge request pipelines.
Valid values are `true` and `false`.

### [Base templates]

Base templates are used by ci template files, we should not import base templates unless the runner files.

* [helpers]: Includes not final templates used by other templates.
* [stages]: Includes common stages (keyword *stages*).
* [use_gitlab_cee_shared_runners]:
Includes the logic to use shared runners at [gitlab.cee].
* [use_kernel_qe_internal_runners]:
Include the logic to use kernel qe runners created by the CKI team,
those runners must be added previosly to the project.
It can be used in both gitlab instances.
* [variables]: Includes common variables.
* [workflow]: Includes all values under *workflow* keyword.

### [CI templates]

Those templates should be imported into `gitlab-ci.yml` files in other repositories.

#### [common]

This file contains common templates:

##### kqe_setup

It create a JSON file consumed by many templates, this job should alwayd be added.
It only runs on merge request pipelines.

##### kqe_markdownlint

It executes [markdownlint] checking markdown files.
If a file *.markdownlint.yaml* exists at the root of the repository, markdownlint will use it.

This job is affected, in merge request pipelines, by the variable `CHECK_ONLY_MR_CHANGES`.

<!-- markdownlint-disable no-duplicate-heading -->
###### CI usage
<!-- markdownlint-restore -->

```yaml
---
include:
  # If you want to use it at gitlab.cee.redhat.com, please change to
  # project: kernel-qe/gitlab-ci-templates
  - project: redhat/centos-stream/tests/kernel/gitlab-ci-templates
    file: .gitlab/all_templates.yml

setup:
  extends: .kqe_setup

markdownlint:
  extends: .kqe_markdownlint
```

<!-- markdownlint-disable no-duplicate-heading -->
###### Manual usage
<!-- markdownlint-restore -->

To run a similar logic outside of the CI, you should run this

```bash
podman run --pull newer --rm -it --volume .:/code:Z quay.io/cki/cki-tools:production markdownlint "*.md"
```

##### kqe_yamllint

It executes [yamllint] checking yaml files.
If a *.yamllint* file exists  at the root of the repository, *yamllint* will use it.

This job is affected, in merge request pipelines, by the variable `CHECK_ONLY_MR_CHANGES`.

<!-- markdownlint-disable no-duplicate-heading -->
###### CI usage
<!-- markdownlint-restore -->

```yaml
---
include:
  # If you want to use it at gitlab.cee.redhat.com, please change to
  # project: kernel-qe/gitlab-ci-templates
  - project: redhat/centos-stream/tests/kernel/gitlab-ci-templates
    file: .gitlab/all_templates.yml

setup:
  extends: .kqe_setup

markdownlint:
  extends: .kqe_yamllint
```

<!-- markdownlint-disable no-duplicate-heading -->
###### Manual usage
<!-- markdownlint-restore -->

To run a similar logic outside of the CI, you should run this

```bash
podman run --pull newer --rm -it --volume .:/code:Z quay.io/cki/cki-tools:production *.yaml
```

### [bash]

This file contains templates related to bash.

#### kqe_mixed_spaces_and_tabs

This job checks mixed spaces and tabs in bash files.

This job is affected, in merge request pipelines, by the variable `CHECK_ONLY_MR_CHANGES`.

<!-- markdownlint-disable no-duplicate-heading -->
##### CI usage
<!-- markdownlint-restore -->

```yaml
---
include:
  # If you want to use it at gitlab.cee.redhat.com, please change to
  # project: kernel-qe/gitlab-ci-templates
  - project: redhat/centos-stream/tests/kernel/gitlab-ci-templates
    file: .gitlab/all_templates.yml

setup:
  extends: .kqe_setup

mixed_spaces_and_tabs:
  extends: .kqe_mixed_spaces_and_tabs
```

<!-- markdownlint-disable no-duplicate-heading -->
###### Manual usage
<!-- markdownlint-restore -->

To run a similar logic outside of the CI, you should run this

```bash
podman run --pull newer --rm -it --volume .:/code:Z quay.io/cki/cki-tools:production bash
comm -12 \
   <(find . -type f -iname '*.sh' -exec grep -lPe '^\t' {} + | sort) \
   <(find . -type f -iname '*.sh' -exec grep -lPe '^ ' {} + | sort)
```

#### kqe_trailing_whitespace

This job checks trailing whitespace in bash files.

This job is affected, in merge request pipelines, by the variable `CHECK_ONLY_MR_CHANGES`.

<!-- markdownlint-disable no-duplicate-heading -->
##### CI usage
<!-- markdownlint-restore -->

```yaml
---
include:
  # If you want to use it at gitlab.cee.redhat.com, please change to
  # project: kernel-qe/gitlab-ci-templates
  - project: redhat/centos-stream/tests/kernel/gitlab-ci-templates
    file: .gitlab/all_templates.yml

setup:
  extends: .kqe_setup

trailing_whitespace:
  extends: .kqe_trailing_whitespace
```

<!-- markdownlint-disable no-duplicate-heading -->
###### Manual usage
<!-- markdownlint-restore -->

To run a similar logic outside of the CI, you should run this

```bash
podman run --pull newer --rm -it --volume .:/code:Z quay.io/cki/cki-tools:production bash
find . -type f -iname '*.sh' -exec grep -lPe '\s$$' {} + || true  | sort
```

#### shellcheck

This job runs [shellcheck]. If the repository has a `.shellcheckrc` this template will use it.
All shellcheck command line options are managed by `SHELLCHECK_OPTS`, more info in [shellcheck_env].

This job is affected, in merge request pipelines, by the variable `CHECK_ONLY_MR_CHANGES`.

<!-- markdownlint-disable no-duplicate-heading -->
##### CI usage
<!-- markdownlint-restore -->

```yaml
---
include:
  # If you want to use it at gitlab.cee.redhat.com, please change to
  # project: kernel-qe/gitlab-ci-templates
  - project: redhat/centos-stream/tests/kernel/gitlab-ci-templates
    file: .gitlab/all_templates.yml

setup:
  extends: .kqe_setup

shellcheck:
  extends: .kqe_shellcheck
```

<!-- markdownlint-disable no-duplicate-heading -->
###### Manual usage
<!-- markdownlint-restore -->

To run a similar logic outside of the CI, you should run this

```bash
podman run --pull newer --rm -it --volume .:/code:Z quay.io/cki/cki-tools:production bash
mkdir /usr/share/beakerlib
touch /usr/share/beakerlib/beakerlib.sh
mkdir /usr/lib/beakerlib
touch /usr/lib/beakerlib/beakerlib.sh
touch /usr/bin/rhts-environment.sh
touch /usr/bin/rhts_environment.sh
shellcheck --source-path=SCRIPTDIR --external-sources -f gcc <bash_file>
```

#### shellspec

This job runs [shellspec], your repository should have a `.shellspec` in the root directory.

This job is *not* affected by the variable `CHECK_ONLY_MR_CHANGES`.

<!-- markdownlint-disable no-duplicate-heading -->
##### CI usage
<!-- markdownlint-restore -->

```yaml
---
include:
  # If you want to use it at gitlab.cee.redhat.com, please change to
  # project: kernel-qe/gitlab-ci-templates
  - project: redhat/centos-stream/tests/kernel/gitlab-ci-templates
    file: .gitlab/all_templates.yml

shellspec:
  extends: .kqe_shellspec
```

<!-- markdownlint-disable no-duplicate-heading -->
###### Manual usage
<!-- markdownlint-restore -->

To run a similar logic outside of the CI, you should run this

```bash
podman run --pull newer --rm -it --volume .:/code:Z quay.io/cki/cki-tools:production shellspec
```

### [python]

This file contains templates related to bash.

### kqe_pylint

This template runs [pylint], pylint command line arguments are defined in the variable `PYLINT_OPTS`, and
if the repository contains any pylint config file in the root of the repository will be used.

This job is affected, in merge request pipelines, by the variable `CHECK_ONLY_MR_CHANGES`.

<!-- markdownlint-disable no-duplicate-heading -->
#### CI usage
<!-- markdownlint-restore -->

```yaml
---
include:
  # If you want to use it at gitlab.cee.redhat.com, please change to
  # project: kernel-qe/gitlab-ci-templates
  - project: redhat/centos-stream/tests/kernel/gitlab-ci-templates
    file: .gitlab/all_templates.yml

setup:
  extends: .kqe_setup

pylint:
  extends: .kqe_pylint
```

<!-- markdownlint-disable no-duplicate-heading -->
#### Manual usage
<!-- markdownlint-restore -->

To run a similar logic outside of the CI, you should run this

```bash
podman run --pull newer --rm -it --volume .:/code:Z quay.io/cki/cki-tools:production pylint file.py
```

### kqe_tox

This template runs [tox], tox command line arguments are defined in the variable `TOX_OPTS`.
This template is *not* affected by the variable `CHECK_ONLY_MR_CHANGES`.

Your tox config should use the coverage directory for output coverage files.

Below you see an example of tox config

```init
...


[testenv:coverage]
deps =
  pytest
  pytest-cov
commands =
  coverage run --source <python-sources> --branch -m pytest --junitxml=coverage/junit.xml --color=yes -v
  coverage report -m
  coverage html -d coverage/
  coverage xml -o coverage/coverage.xml

...
```

<!-- markdownlint-disable no-duplicate-heading -->
#### CI usage
<!-- markdownlint-restore -->

```yaml
---
include:
  # If you want to use it at gitlab.cee.redhat.com, please change to
  # project: kernel-qe/gitlab-ci-templates
  - project: redhat/centos-stream/tests/kernel/gitlab-ci-templates
    file: .gitlab/all_templates.yml

setup:
  extends: .kqe_setup

tox:
  extends: .kqe_tox
```

<!-- markdownlint-disable no-duplicate-heading -->
#### Manual usage
<!-- markdownlint-restore -->

To run a similar logic outside of the CI, you should run this

```bash
podman run --pull newer --rm -it --volume .:/code:Z quay.io/cki/cki-tools:production tox
```

### [tmt][tmt_directory]

This file contains templates related to [tmt].

#### check polarion IDs

A tmt test could have an `id` field, this field should be uniqued and use the UUID format.

This template check for duplicated and bad formatted IDs.

<!-- markdownlint-disable no-duplicate-heading -->
##### CI usage
<!-- markdownlint-restore -->

```yaml
---
include:
  # If you want to use it at gitlab.cee.redhat.com, please change to
  # project: kernel-qe/gitlab-ci-templates
  - project: redhat/centos-stream/tests/kernel/gitlab-ci-templates
    file: .gitlab/all_templates.yml

check_polarion_ids:
  extends: .kqe_check_polarion_ids
```
<!-- markdownlint-disable no-duplicate-heading -->
##### Manual usage
<!-- markdownlint-restore -->

To run a similar logic outside of the CI, you should run this

<!-- markdownlint-disable line-length -->
```bash
tmt tests export --how json > tmt_tests.json
podman run --pull newer --rm -it --volume .:/code:Z registry.gitlab.com/redhat/centos-stream/tests/kernel/kernel-qe-tools/kernel-qe-tools:latest \
  check_polarion_ids --input tmt_tests.json
```
<!-- markdownlint-restore -->

#### tmt lint

This template runs tmt lint.

This job is affected, in merge request pipelines, by the variable `CHECK_ONLY_MR_CHANGES`.

<!-- markdownlint-disable no-duplicate-heading -->
##### CI usage
<!-- markdownlint-restore -->

```yaml
---
include:
  # If you want to use it at gitlab.cee.redhat.com, please change to
  # project: kernel-qe/gitlab-ci-templates
  - project: redhat/centos-stream/tests/kernel/gitlab-ci-templates
    file: .gitlab/all_templates.yml

tmt_lint:
  extends: .kqe_tmt_lint
```

<!-- markdownlint-disable no-duplicate-heading -->
###### Manual usage
<!-- markdownlint-restore -->

To run a similar logic outside of the CI, you should run this

```bash
podman run --pull newer --rm -it --volume .:/code:Z quay.io/cki/cki-tools:production tmt lint --outcome-only fail <directory>
```

#### tmt plan documentation

This template generates test plans documentation based on tmt plans.

<!-- markdownlint-disable no-duplicate-heading -->
##### CI usage
<!-- markdownlint-restore -->

```yaml
---
include:
  # If you want to use it at gitlab.cee.redhat.com, please change to
  # project: kernel-qe/gitlab-ci-templates
  - project: redhat/centos-stream/tests/kernel/gitlab-ci-templates
    file: .gitlab/all_templates.yml

plan_documentation:
  extends: .kqe_check_tmt_plan_documentation
```

<!-- markdownlint-disable no-duplicate-heading -->
###### Manual usage
<!-- markdownlint-restore -->

To run a similar logic outside of the CI, you should run this

```bash
podman run --pull newer --rm -it --volume .:/test-plans:Z quay.io/cki/cki-tools:production \
  python3 -m cki_tools.tmt_generate_test_plans_doc --plans-dir /test-plans
```

#### testing farm

This template use [testing farm] to run test plan defined in the repository.
It uses the following variables:

* `TESTING_FARM_API_TOKEN`: your token.
* `TESTING_FARM_API_URL`: Testing farm API (by default defined in the container image).
* `TESTING_FARM_ARCH`: architecture used by Testing Farm (by default `x86_64`).
* `TESTING_FARM_COMPOSE`: compose used by Testing Farm (by default `CentOS-Stream-9`).
* `TESTING_FARM_PLAN_PATH`: path to the testing-farm path (by default `testing-farm/sanity.fmf`).

<!-- markdownlint-disable no-duplicate-heading -->
##### CI usage
<!-- markdownlint-restore -->

```yaml
---
include:
  # If you want to use it at gitlab.cee.redhat.com, please change to
  # project: kernel-qe/gitlab-ci-templates
  - project: redhat/centos-stream/tests/kernel/gitlab-ci-templates
    file: .gitlab/all_templates.yml

testing_farm:
  extends: .kqe_testing_farm
```

<!-- markdownlint-disable no-duplicate-heading -->
###### Manual usage
<!-- markdownlint-restore -->

To run a similar logic outside of the CI, you should run this

```bash
podman run --pull newer --rm -it --volume .:/code:Z quay.io/testing-farm/cli:latest bash
export TESTING_FARM_API_TOKEN=...
testing-farm request --compose CentOS-Stream-9 --git-url <your_repository> --git-ref <your_branch> --plan <your_plan> # /testing-farm/sanity
```

[kernel-qe/gitlab-ci-templates]: https://gitlab.cee.redhat.com/kernel-qe/gitlab-ci-templates
[DRY pattern]: https://en.wikipedia.org/wiki/Don%27t_repeat_yourself
[templates]: https://docs.gitlab.com/ee/development/cicd/templates.html
[Base templates]: .gitlab/base_templates
[helpers]: .gitlab/base_templates/helpers.yml
[stages]: .gitlab/base_templates/stages.yml
[use_gitlab_cee_shared_runners]: .gitlab/base_templates/use_gitlab_cee_shared_runners.yml
[gitlab.cee]: https//gitlab.cee.redhat.com
[use_kernel_qe_internal_runners]: .gitlab/base_templates/use_kernel_qe_internal_runners.yml
[variables]: .gitlab_templates/base_templates/variables
[workflow]: .gitlab/base_templates/workflow.yml
[CI templates]: ./gitlab/ci_templates
[common]: .gitlab/ci_templates/common.yml
[tmt_directory]: .gitlab/ci_templates/common.yml
[yamllint]: https://yamllint.readthedocs.io/en/stable/
[markdownlint]: https://github.com/igorshubovych/markdownlint-cli
[bash]: .gitlab/ci_templates/bash.yml
[python]: .gitlab/ci_templates/python.yml
[shellcheck]: https://github.com/koalaman/shellcheck
[shellspec]: https://shellspec.info/
[tmt]: https://tmt.readthedocs.io/en/stable/examples.html
[testing farm]: https://docs.testing-farm.io
[shellcheck_env]: https://github.com/koalaman/shellcheck/blob/master/shellcheck.1.md#environment-variables
[tox]: https://tox.wiki
[pylint]: https://pypi.org/project/pylint
